# `socl`

[![dependency status][deps.rs-svg]][deps.rs][![pipeline status]][pipeline]

A quick cli for stackoverflow, answering all your questions!
Super-heavily inspired by [`howdoi`][howdoi], a super usefull python script.


## Why Rust?

- Because Rust.
  - To be honnest, because I want to learn this language, and Rewriting It In Rust was a simple task
- Because a static binary (if I manage to build on all three main platform with gitlab-ci).
- Because speed. For now, faster than `howdoi` whithout using cache (neither on `howdoi` nor `socl`
side).

[`hyperfine`][hyperfine] benchmark:
```
$ hyperfine -m 10 -w 1 -p 'howdoi -C' 'howdoi es6 classes -n 3' 'target/release/socl es6 classes -n 3'
Benchmark #1: howdoi es6 classes -n 3

  Time (mean ± σ):      2.055 s ±  0.182 s    [User: 484.4 ms, System: 81.6 ms]
 
  Range (min … max):    1.876 s …  2.432 s
 
Benchmark #2: target/release/socl es6 classes -n 3

  Time (mean ± σ):     962.0 ms ±  88.7 ms    [User: 81.2 ms, System: 17.2 ms]
 
  Range (min … max):   810.7 ms … 1119.2 ms
```


[howdoi]: https://github.com/gleitz/howdoi
[hyperfine]: https://github.com/sharkdp/hyperfine
[deps.rs-svg]: https://deps.rs/repo/gitlab/justinrlle/socl/status.svg
[deps.rs]: https://deps.rs/repo/gitlab/justinrlle/socl
[pipeline status]: https://gitlab.com/justinrlle/socl/badges/master/pipeline.svg
[pipeline]: https://gitlab.com/justinrlle/socl/commits/master
