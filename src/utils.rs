macro_rules! search_url {
    ($s:expr, $q:expr) => {{
        use url::percent_encoding::{utf8_percent_encode, DEFAULT_ENCODE_SET};
        format!( "https://www.google.fr/search?q=site:{}%20{}",
                 $s, utf8_percent_encode($q, DEFAULT_ENCODE_SET).to_string())
    }};
    ($s:expr, $q:expr, SSL = $ssl:expr) => {{
        use url::percent_encoding::{utf8_percent_encode, DEFAULT_ENCODE_SET};
        if $ssl {
            format!("https://www.google.fr/search?q=site:{}%20{}",
                   $s, utf8_percent_encode($q, DEFAULT_ENCODE_SET).to_string())
        } else {
            format!("http://www.google.fr/search?q=site:{}%20{}",
                   $s, utf8_percent_encode($q, DEFAULT_ENCODE_SET).to_string())
        }
    }};
}
