use scraper::Selector;

lazy_static! {
    pub static ref LINKS: Selector = Selector::parse("div.srg > div.g h3.r > a").unwrap();
    pub static ref ANSWERS: Selector = Selector::parse("#answers .answer").unwrap();
    pub static ref CODE: Selector = Selector::parse(".answercell .post-text pre>code").unwrap();
    pub static ref CONTENT: Selector = Selector::parse(".answercell .post-text").unwrap();
}
