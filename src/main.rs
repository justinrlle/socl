#[macro_use] extern crate failure;
#[macro_use] extern crate lazy_static;
extern crate proxy_config;
extern crate reqwest;
extern crate scraper;
#[macro_use] extern crate structopt;
extern crate url;

#[macro_use] mod utils;
mod scrap;
mod selectors;

use scrap::{QueryOptions, AnswerKind};
use structopt::StructOpt;

const DEFAULT_UA: &str = "Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0";
const DEFAULT_SITE: &str = "stackoverflow.com";

#[derive(StructOpt, Debug, Clone)]
#[structopt(raw(setting = "structopt::clap::AppSettings::ColoredHelp"))]
pub struct Options {
    /// Number or answers to return, default to 1
    #[structopt(short = "n", long = "number", default_value = "1")]
    pub number: u32,

    /// The question to answer
    pub query: Vec<String>,

    /// If set, retrieve the full answer. Override --code
    #[structopt(short = "f", long = "full")]
    pub full: bool,

    /// If set, retrieve all block of code in the answer
    #[structopt(short = "c", long = "code")]
    pub code: bool,

    /// The position of the answer, defaults to 1
    #[structopt(short = "p", long = "position", default_value = "1")]
    pub position: u32,
}

#[derive(Debug, Fail)]
pub enum SoclError {
    #[fail(display = "An error occured: {}", msg)]
    Any { 
        msg: String
    },
}

pub type SoclResult<T> = Result<T, failure::Error>;

fn build_client() -> SoclResult<reqwest::Client> {
    let mut client_builder = reqwest::Client::builder();
    if let Ok(conf) = proxy_config::get_proxy_config() {
        if conf.proxies.contains_key("https") {
            client_builder.proxy(reqwest::Proxy::https(conf.proxies["https"].clone())?);
        }
        if conf.proxies.contains_key("http") {
            client_builder.proxy(reqwest::Proxy::http(conf.proxies["http"].clone())?);
        }
    };
    client_builder.build().map_err(Into::into)
}

fn program(opts: &Options) -> SoclResult<()> {
    let client = build_client()?;
    let search_url = search_url!(DEFAULT_SITE, &opts.query.join(" "));
    let links = scrap::google_search(&client, &search_url, DEFAULT_UA, opts.number)?;
    let kind = if opts.full {
        AnswerKind::Full
    } else if opts.code {
        AnswerKind::FullCode
    } else {
        AnswerKind::FirstCode
    };
    let query_opts = QueryOptions {
        position: opts.position,
        kind,
    };
    let stdout = std::io::stdout();
    let mut lock = stdout.lock();
    for (i, link) in links.iter().enumerate() {
        let answer = scrap::so_query(&client, link, DEFAULT_UA, &query_opts)?;
        if let Some(answer) = answer {
            answer.print(&mut lock, i, opts.number > 1)?;
        }
    }
    Ok(())
}

fn main() {
    let opts = Options::from_args();

    if let Err(err) = program(&opts) {
            eprintln!("{}: {}", Options::clap().get_name(), err);
            ::std::process::exit(2);
    }
}
