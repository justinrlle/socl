use std::io::{Read, Write};
use std::io::Result as IoResult;
use std::ops::Add;

use reqwest::{Client, Response};
use reqwest::header::UserAgent;
use scraper::{Html, ElementRef};

use super::{SoclError, SoclResult};
use super::selectors;

#[derive(Clone, Debug)]
pub struct Answer {
    id: String,
    content: String,
}

impl Answer {
    fn from_element_ref(el: ElementRef, kind: AnswerKind) -> Self {
        let id = el.value().attr("data-answerid").unwrap_or("").to_owned();
        let content = retrieve_content(&el, kind);
        Answer { id, content }
    }

    pub fn print<W: Write>(&self, out: &mut W, index: usize, show_header: bool) -> IoResult<()> {
        if show_header {
            out.write_fmt(format_args!(
                "-- Answer {i}: {id} --\n",
                i = index,
                id = self.id
            ))?;
        }
        out.write_fmt(format_args!("{}\n", self.content)).map(
            |_| (),
        )
    }
}

#[derive(Clone, Debug)]
pub struct QueryOptions {
    pub position: u32,
    pub kind: AnswerKind,
}

#[derive(Clone, Copy, Debug)]
pub enum AnswerKind {
    FirstCode,
    FullCode,
    Full,
}

impl Default for AnswerKind {
    fn default() -> AnswerKind {
        AnswerKind::FirstCode
    }
}

pub fn google_search(
    client: &Client,
    search_url: &str,
    user_agent: &'static str,
    number: u32,
) -> SoclResult<Vec<String>> {
    let mut resp = client
        .get(search_url)
        .header(UserAgent::new(user_agent))
        .send()?;
    if !resp.status().is_success() {
        Err(SoclError::Any {
            msg: format!("Request at {} got responded with a {} status code",
                    resp.url(), resp.status())
        }.into())
    } else {
        let content = get_content(&mut resp)?;
        let html = Html::parse_document(&content);
        let links: Option<Vec<String>> = html.select(&selectors::LINKS)
            .take(number as usize)
            .map(|e| e.value().attr("href").map(str::to_owned))
            .collect();
        links.ok_or_else(|| {
            SoclError::Any {
                msg: format!("Request ar {} didn't find any links", resp.url())
            }.into()
        })
    }
}

pub fn so_query(
    client: &Client,
    answer_url: &str,
    user_agent: &'static str,
    opts: &QueryOptions,
) -> SoclResult<Option<Answer>> {
    let mut resp = client
        .get(answer_url)
        .header(UserAgent::new(user_agent))
        .send()?;
    if !resp.status().is_success() {
        Err(SoclError::Any {
            msg: format!("Request at {} got responded with a {} status code",
                    resp.url(), resp.status())
        }.into())
    } else {
        let content = get_content(&mut resp)?;
        let html = Html::parse_document(&content);
        let mut answers = html.select(&selectors::ANSWERS);
        Ok(answers.nth(opts.position as usize).map(|e| {
            Answer::from_element_ref(e, opts.kind)
        }))
    }
}

fn retrieve_content(answer: &ElementRef, kind: AnswerKind) -> String {
    fn concat_code_spans(el: ElementRef) -> String {
        el.text().fold(String::new(), |l, r| if l.is_empty() {
            l + r
        } else {
            (l + " ") + r
        })
    };
    match kind {
        AnswerKind::FirstCode => {
            answer
                .select(&selectors::CODE)
                .next()
                .map(concat_code_spans)
                .unwrap_or_else(|| retrieve_content(answer, AnswerKind::Full))
        }
        AnswerKind::FullCode => {
            let content = answer
                .select(&selectors::CODE)
                .map(concat_code_spans)
                .fold(String::new(), |l, r| l + "\n" + &r);

            if !content.is_empty() {
                content
            } else {
                retrieve_content(answer, AnswerKind::Full)
            }
        }
        AnswerKind::Full => {
            answer
                .select(&selectors::CONTENT)
                .map(|el| el.text().fold(String::new(), String::add))
                .fold(String::new(), |l, r| l + &r)
        }
    }
}

#[inline]
fn get_content(resp: &mut Response) -> SoclResult<String> {
    let mut content = String::new();
    resp.read_to_string(&mut content)?;
    Ok(content)
}
